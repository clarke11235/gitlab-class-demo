# Plotter Function

The plotter will create an x-y plot
Reasons this is **fantastic**:
* We are using python
* Plots are awesome
* We are using Git and GitLab

## Here is a link
It's a [hyperlink](https://www.google.com/)

## Here is a picture
![AFMC Logo](https://www.edwards.af.mil/Portals/50/EAFBlogo.png?ver=tpSZ6ni8d00SrFmQS1MAPg%3d%3d)